package nivelamentotiposdados;

import java.util.Scanner;

public class ParImpar {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite um número");
        Double n1 = scanner.nextDouble();

        if (n1 % 2 == 0){
            System.out.println("É par");
        }else{
            System.out.println("É ímpar");
        }
    }
}
