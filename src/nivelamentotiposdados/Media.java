package nivelamentotiposdados;

import java.util.Scanner;

public class Media {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Digite o valor de n1");
        Double n1 = scanner.nextDouble();

        System.out.println("Digite o valor de n2");
        Double n2 = scanner.nextDouble();

        Double media = (n1 + n2) /2;
        System.out.println("A média é " + media);
    }
}
