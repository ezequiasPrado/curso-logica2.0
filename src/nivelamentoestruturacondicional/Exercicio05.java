package nivelamentoestruturacondicional;

import java.util.Scanner;

public class Exercicio05 {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Digite o nome do aluno");
        String nomeAluno = scanner.nextLine();
        System.out.println("Digite a primeira nota");
        Double nota1 = scanner.nextDouble();
        System.out.println("Digite a segunda nota");
        Double nota2 = scanner.nextDouble();
        System.out.println("Digite a terceira nota");
        Double nota3 = scanner.nextDouble();

        Double media = (nota1 + nota2 + nota3) / 3;

        if (media >= 7) {
            System.out.println("O aluno " + nomeAluno + " está aprovado");
        }else{
            System.out.println("O aluno " + nomeAluno + " está reprovado");
        }
    }
}
