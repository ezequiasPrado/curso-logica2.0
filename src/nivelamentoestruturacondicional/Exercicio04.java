package nivelamentoestruturacondicional;

import java.util.Scanner;

public class Exercicio04 {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Digite a operação + - / *");
        String operacao = scanner.nextLine();

        System.out.println("Digite o valor de a");
        Double a = scanner.nextDouble();

        System.out.println("Digite o valor de b");
        Double b = scanner.nextDouble();

        if (operacao.equals("+")){
            System.out.println(a + b);
        } else if (operacao.equals("-")) {
            System.out.println(a - b);
        } else if (operacao.equals("*")) {
            System.out.println(a * b);
        } else if (operacao.equals("/")) {
            System.out.println(a / b);
        }

    }
}
